(function(){
    $(document).on("deviceready", function(){
        var preLoaded = window.localStorage.preLoaded;
        if (preLoaded) {
            window.location = "home.html";
        }
        else {
            $('.first-form-wrapper').slideDown(1000);
            
            $('#continue').on('tap', function(){
                var pNum = $('#number-input').val();
                var valid = (pNum.length == 10) && !isNaN(pNum.replace('e',''));
                if (valid) {
                    window.localStorage.preLoaded = true;
                    window.localStorage.mobileNumber = pNum;
                    window.location = "home.html";
                }
                else {
                    navigator.notification.alert("Please enter a valid 10 digit mobile number");
                }
            });
            
            $('#skip').on('tap', function(){
                window.localStorage.preLoaded = true;
                window.location = "home.html";
            });
        }
    });
})();