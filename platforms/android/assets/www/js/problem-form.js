(function(){
    $(document).on('deviceready', function(){
        var hash = window.location.hash.replace('#','').split('|');
        var category = hash[0].trim();
        var subcategory = hash[1].trim();
        
        $('input[name=category]').val(category);
        $('#head-category').text(category);
        
        if (subcategory != "") {
            $('input[name=sub_category]').val(subcategory);
            $('#head-sub-category').text(" : "+subcategory);
        }
        
        var mobile = window.localStorage.mobileNumber;
        $('input[name=mobile]').val(mobile);
        
        getLocation();
        
        $('.from-camera').on('tap',function(){
            navigator.camera.getPicture(onCameraSuccess, onCameraFail, {
                quality: 75,
                sourceType : Camera.PictureSourceType.CAMERA,
                destinationType: Camera.DestinationType.DATA_URL
            });
        });
        $('.from-file').on('tap',function(){
            navigator.camera.getPicture(onCameraSuccess, onCameraFail, {
                quality: 75,
                sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
                destinationType: Camera.DestinationType.DATA_URL
            });
        });
        $('#submit-form').on('tap',function(){
            $('input[name=comments]').val( $('textarea').val() );
            var data = new FormData( document.getElementById('form') ) ;
            $(this).attr('disabled','disabled');
            $.ajax({
                'url': base_url + 'post.php',
                'type':'POST',
                'data': data,
                'success': function (response) {
                        $('#submit-form').removeAttr('disabled');
                        /*
                        if (!window.localStorage.statusData) {
                            navigator.notification.alert('Inside iff')
                            window.localStorage.statusData = new Array();
                        }
                        var data = new Object();
                        data["complaintNum"] = response['complaint_no'];
                        data['location'] = response['address'];
                        data['status'] = response['status'];
                        
                        data.img = $('input[name=image]').val();
                        data['desc'] = $('input[name=comments]').val();
                        
                        navigator.notification.alert( "data -> " + JSON.stringify(data) );
                        window.localStorage.statusData.push(data);
                        */
                        navigator.notification.alert("Thanks for submitting a complaint!");
                        window.location="home.html";
                },
                processData: false,
                contentType: false,
                'error':function(response) {
                    $('#submit-form').removeAttr('disabled');
                    navigator.notification.alert('Some error occured, please try again');
                }
            });
        });
        
        $(document).on("buttonback", function(){
            window.location = "problem.html";
        });
    });
        
    var getLocation = function() {
        navigator.geolocation.getCurrentPosition(function(position){
            var lat = position.coords.latitude;
            var longi = position.coords.longitude;
            $('input[name=latitude]').val(lat);
            $('input[name=longitude]').val(longi);
            var frame = document.getElementById('map-iframe');
            try {
                frame.src = base_url + "test2.php?lat=" + lat + "&long=" + longi;
                $('#location-text').hide();
            }
            catch(e) {
                onLocationFail();
            }
            
        }, onLocationFail,
        { maximumAge: 20000, timeout: 20000, enableHighAccuracy: true }
        );
    }
    
    var onLocationFail = function() {
        $('#location-text').hide();
        $('#to-show').show();
        navigator.notification.alert("Sorry, we are unable to retrive your location. Please enter a custom location");
    }
    
    
    var onCameraSuccess = function(imageData) {
        var img = 'data:image/jpeg;base64,' + imageData;
        $('input[name=image]').val(img);
        $('.image-tile').css('background-image', 'url('+img+')');
        $('.image-sub-tile').hide();
        $('.btn-small-wrap').show();
    }
    
    var onCameraFail = function(message) {
        navigator.notification.alert(message, 'Failed selecting image');
    }
})();
