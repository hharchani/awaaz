(function(){
    $(document).on('deviceready',function(){
        $(document).on("buttonback", function(){
            navigator.app.backHistory();
        });
        
        $('input[name=q1]').on('click tap',function(){
            $('label.btn.active').removeClass('btn-info').removeClass('active').addClass('btn-default');
            $(this).closest('label').removeClass('btn-default').addClass('btn-info').addClass('active');
        });
        
        $('#submit-form').on('tap',function(){
            var data = new FormData( document.getElementById('form') ) ;
            $(this).attr('disabled','disabled');
            $.ajax({
                'url':base_url + 'feedback.php',
                'type':'POST',
                'data': data,
                'success': function (response) {
                        $('#submit-form').removeAttr('disabled');
                        navigator.notification.alert("Thanks for your response");
                        window.location="home.html";
                    },
                processData: false,
                contentType: false,
                'error':function() {
                    $('#submit-form').removeAttr('disabled');
                    navigator.notification.alert('error');
                }
            });
        });
    });
})();