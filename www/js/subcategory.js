var sub = {
    'Garbage':[
        'Sweeping not done',
        'Garbage not lifted',
        'Removal of dead animals'
    ],
    'Electricity':[
        'Theft',
        'No supply'
    ],
    'Street Lights':[
    ],
    'Roads':[
        'Potholes',
        'Speed breakers',
        'Wrong Parking',
        'Regular Traffic Jams'
    ],
    'Water':[
        'Drinking Water',
    ],
    'Drainage':[
        'Stagnation',
        'Leak',
        'Overflow',
        'Open man holes'
    ],
    'Stray Animals':[
    ],
    'Pollution':[
        'Factories (Smoke/Smell)',
        'Vehicals'
    ],
    'Eve Teasing':[
        'Rude behaviour',
        'Anti social',
        'Nuisance'
    ],
    'Others':[  
    ],
}