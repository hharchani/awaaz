var dictionary = {
    'team':['Ⓒ  Team Minion','Ⓒ  टीम मिनियन' ],
    'app':['Smart City','स्मार्ट सिटी'],
    'welcome':['Welcome', 'आपका स्वागत है'],
    'enter-mob-no':['Enter you 10 digit mobile number','अपना १० अंकों का मोबाइल नंबर दर्ज करे'],
    'continue':['Continue', 'आगे चले'],
    'skip':['Skip this step', 'इस कदम को छोड़े']
};