var sub = {
    'Garbage':[
        'Sweeping not done',
        'Garbage not lifted',
        'Removal of dead animals'
    ],
    'Electricity':[
        'Theft',
        'No supply'
    ],
    'Street Lights':[
    ],
    'Roads':[
        'Potholes',
        'Speed breakers',
        'Wrong Parking',
        'Regular Traffic Jams'
    ],
    'Water':[
        'Drinking Water',
    ],
    'Drainage':[
        'Stagnation',
        'Leak',
        'Overflow',
        'Open man holes'
    ],
    'Stray Animals':[
    ],
    'Pollution':[
        'Factories - Smoke/Smell',
        'Vehicles'
    ],
    'Eve Teasing':[
        'Rude behaviour',
        'Anti social',
        'Nuisance'
    ],
    'Others':[
    ],
};
var onSubClick = function(e){
    var cat = e.data.cat;
    var sub = e.data.sub;
    window.location = 'problem-form.html#' + cat + '|' + sub;
};
(function(){
    $(document).on('ready', function(){
        
        var subList = $('<div class="col-xs-12">').css('display','none');
        var currentOpen = null;
        $('.problem-tile').on('tap', function(){
            if ( sub[this.id].length ) {
                if (currentOpen == this.id) {
                    currentOpen = null;
                    subList.slideUp();
                    $('.current-open').removeClass('current-open');
                    return;
                }
                var subArray = sub[this.id];
                subList.finish().detach().empty().css('display','none')
                .appendTo( $(this).closest('.row.row-centered') );
                var alignment = !($(this).prev().length)? 'left' : 'right';
                for(var i=0; i<subArray.length; i++) {
                    subList.append(
                        $('<div class="sub-item">'+ subArray[i] +'</div>')
                        .css('text-align',alignment)
                        .on('tap',{cat:this.id, sub:subArray[i]},onSubClick)
                    );
                }
                subList.slideDown();
                currentOpen = this.id;
                $('.current-open').removeClass('current-open');
                $(this).addClass('current-open');
            }
            else {
                subList.slideUp(200);
                window.location = 'problem-form.html#' + this.id + '|';
                
            }
        });
    });
    $(document).on("buttonback", function(){
        navigator.app.backHistory();
    });
})();